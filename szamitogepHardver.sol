pragma solidity ^0.6.6;

contract szamitogepHardver{
    uint azon;
    
    mapping(uint=>Hardver) private hardverek;
    
    mapping(address=>bool) private adminok;
    mapping(address=>bool) private gyartok;
    mapping(address=>bool) private szallitok;
    mapping(address=>bool) private viszonteladok;
    
    struct Hardver{
        HardverTipus hardverTipusa;
        address gyarto;
        address[] szallitok;        
        address viszontelado;
    }
    
    enum HardverTipus{
        eger,
        billentyuzet,
        monitor,
        headset
    }
    
    constructor() public{
       adminok[msg.sender]=true;  //a contract telepitoje admin lesz
       azon=1;
    }
    
    modifier OnlyAdmin(){
         require(adminok[msg.sender]==true, "Ezt a fuggvenyt csak adminok hivhatjak");
         _;
    }
    modifier OnlyGyarto(){
         require(gyartok[msg.sender]==true, "Ezt a fuggvenyt csak gyartok hivhatjak");
         _;
    }
    modifier OnlySzallito(){
         require(szallitok[msg.sender]==true, "Ezt a fuggvenyt csak szallitok hivhatjak");
         _;
    }
    modifier OnlyViszontelado(){
         require(viszonteladok[msg.sender]==true, "Ezt a fuggvenyt csak viszonteladok hivhatjak");
         _;
    }
    
    //hardverek hozzaadasa
    function ujHardverHozzaad(HardverTipus _hardverTipusa) public OnlyGyarto()
    {
        uint _ID=azon;
        azon++;
        hardverek[_ID].hardverTipusa=_hardverTipusa;
        hardverek[_ID].gyarto=msg.sender;
    }
    function hardverhezSzallito(uint _ID) public OnlySzallito()
    {
         require(hardverek[_ID].gyarto!=address(0) , "A termek nem letezik");
         require(hardverek[_ID].viszontelado==address(0) , "A termek mar megerkezett a celba");
         hardverek[_ID].szallitok.push(msg.sender);
    }
    function hardverhezViszontelado(uint _ID) public OnlyViszontelado()
    {
         require(hardverek[_ID].szallitok.length>0 , "A termek meg a gyartonal van");  
         require(hardverek[_ID].viszontelado==address(0) , "A termeket mar atvettek");
         hardverek[_ID].viszontelado=msg.sender;
    }
    
    
    //roleok modositasa
    function adminMod(address _ujAdmin, bool _statusz) public OnlyAdmin()
    {
         adminok[_ujAdmin]=_statusz;
    }
    function gyartoMod(address _ujGyarto, bool _statusz) public OnlyAdmin()
    {
         gyartok[_ujGyarto]=_statusz;
    }
    function szallitoMod(address _ujSzallito, bool _statusz) public OnlyAdmin()
    {
         szallitok[_ujSzallito]=_statusz;
    }
    function viszonteladoMod(address _ujViszontelado, bool _statusz) public OnlyAdmin()
    {
        
         viszonteladok[_ujViszontelado]=_statusz;
    }
    
    // lekerdezesek
    function statuszLeker() public view returns(bool _adminStat, bool _gyartoStat, bool _szallitoStat, bool _viszonteladoStat){
        _adminStat=adminok[msg.sender];
        _gyartoStat=gyartok[msg.sender];
        _szallitoStat=szallitok[msg.sender];
        _viszonteladoStat=viszonteladok[msg.sender];
    }
    
    function statuszLekerAdmin(address _user) public OnlyAdmin() 
        view returns(bool _adminStat, bool _gyartoStat, bool _szallitoStat, bool _viszonteladoStat){
        _adminStat=adminok[_user];
        _gyartoStat=gyartok[_user];
        _szallitoStat=szallitok[_user];
        _viszonteladoStat=viszonteladok[_user];
    }
    
    function hardverLeker(uint _ID) public OnlyAdmin() view returns(string memory _termekTipus, address _gyarto, address[] memory _szallitok, address _viszontelado) 
    {
        if(hardverek[_ID].hardverTipusa==HardverTipus.eger)
        {
            _termekTipus="Eger";
        }
        else if(hardverek[_ID].hardverTipusa==HardverTipus.billentyuzet)
        {
            _termekTipus="Billentyuzet";
        }
        else if(hardverek[_ID].hardverTipusa==HardverTipus.monitor)
        {
            _termekTipus="Monitor";
        }
        else if(hardverek[_ID].hardverTipusa==HardverTipus.headset)
        {
            _termekTipus="Headset";
        }
        
        _gyarto=hardverek[_ID].gyarto;
        _szallitok=hardverek[_ID].szallitok;
        _viszontelado=hardverek[_ID].viszontelado;
    }
    
    
}
    